<?php
    include_once 'classes/User.php';
    include_once 'classes/JWT.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $user = new User();

    $key = 'secret';

    if($request->action === 'login') {
        if(isset($request->email) && isset($request->password)) {

            $userData = $user->Login($request->email, $request->password);
            if ($userData->error) {
                echo json_encode($userData);
            } else {
                $token = array(
                    'iss' => 'backend.credito',
                    'sub' => 'user',
                    'user'=> $userData,
                );
                $jwt = JWT::encode($token, $key);
                $data = array(
                    'token' => $jwt,
                );
                echo json_encode($data);
            }

        } else {
            $data = array(
                'error' => 'please fill in all required fields',
            );
            echo json_encode($data);
        }
    } else if($request->action === 'getUserData') {
        try {
            $data = JWT::decode($request->token, $key, array('HS256'));
            if($data->iss === 'backend.credito' && $data->sub === 'user') {
                $data = $data->user;
                echo json_encode($data);
            } else {
                $data = array(
                    'error' => 'invalid token or key',
                );
                echo json_encode($data);
            }
        } catch (\Exception $e) {
            $data = array(
                'error' => 'invalid token or key',
            );
            echo json_encode($data);
        }
    } else {
        $data = array(
            'error' => 'error: invalid action',
        );
        echo json_encode($data);
    }
    
?>
