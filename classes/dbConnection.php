<?php

class Connection {
    public function dbConnect() {
        try {
            $conn = new PDO('mysql:host='.getenv('MYSQL_HOST').';port='.getenv('MYSQL_PORT').';dbname='.getenv('MYSQL_DATABASE').'', getenv('MYSQL_USER'), getenv('MYSQL_PASSWORD'));
        }
        catch (PDOException $e) {
            die("Connection failed:" . $e->getMessage());
        }
        return $conn;
    }
}
?>
