<?php
include_once '../functions.php';
include_once 'dbConnection.php';

class User {
    private $db;

    public function __construct() {
        $this->db = new Connection();
        $this->db = $this->db->dbConnect();
    }

    public function Login($email, $pass) {
        if(!empty($email) && !empty($pass)) {
            $sql = "SELECT gebruiker.gebruikerID, concat(gebruiker.voornaam, ' ', IFNULL(gebruiker.tussenvoegsel,''), ' ', gebruiker.achternaam) 'naam', gebruiker.email, gebruiker.wachtwoord , rol.rolDsc FROM gebruiker INNER JOIN rol ON rol.rolID = gebruiker.rolID WHERE email = :mail";
            $statement = $this->db->prepare($sql);

            $statement->bindParam(':mail', $email);
            $statement->execute();
            $results = $statement->fetch(PDO::FETCH_ASSOC);

            // nette manier (gehashed wachtwoord)
            //if(count($results) > 0 && password_verify($pass, $results['wachtwoord'])) {
            if(count($results) > 0 && $pass === $results['wachtwoord']) {
                unset($results['wachtwoord']);
            } else {
                $results = array(
                    'error' => 'Invaldid username or password',
                );
            }
        }
        return $results;
    }
}
?>
